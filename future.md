# Дальнейшее развитие

1. Предлагается разделить уровни данных и представлений, а именно в место поля ``data`` использовать провайдер данных


Примерный концепт провайдера

```js
function DataProvider(options) {
    this.data = [];
    // размер страницы, для организации пагинации
    this.pageSize = 25;
    // поле по которому производится сортировка
    this.sortField = 'id';
    
    // Загрузка данных в провайдер
    this.loadData = function (data) {}
    
    /**
    * возврат данных из провайдера
    * @param offset - смещение
    * @param filter - фильтрующее выражение
    */
    this.getRows = function (offset, filter) {
        if (offset === undefined) {
            offset = 0;
        }
        data = this.data;
        if (filter) {
            // фильтрация data
        }
        data = data.sort(); // или функция itemSortCompare для сложных сортировок
        // организация страницы
        data = data.slice(offset, offset + this.pageSize);
        return data;
    }
}
```

В самом классе компонента

```js
this.provider = new DataProvider(options)
```

2. Для произвольного отображения в списке уже задан параметр ``displayKeys``, 
но для сложных случаев есть возможность переопределить метод ``getItemValue``