﻿function ListView(options) {
    var defaults = {
            width: 250,
            height: 500,
            displayKeys: [],
            displaySeparator: ' ',
            groupKey: ''
        },
        self = this;
    this._id = null;
    this.$_element = null;
    this.$_listElement = null;
    this.data = {};
    this.$_groupElements = null;
        
    $.each(defaults, function (key, value) {
        self[key] = (options[key] === undefined) ? value : options[key];
    })
}

ListView.prototype.generateId = function () {
    return 'id_' + Math.floor(Math.random() * 16777215).toString(16);
};

ListView.prototype.getId = function () {
    return this._id;
};

ListView.prototype.getElement = function () {
    if (!this.$_element) {
        this.render();
    }
    return this.$_element;
};

/**
* Функция рендеринга компонента
*/
ListView.prototype.render = function () {
    this.$_element = $('<div></div>');
    this._id = this.generateId();
    this.$_element.attr('id', this.getId());
    this.$_element.width(this.width);
    this.$_element.height(this.height);
    this.$_element.addClass('listview');
    
    this.$_listElement = $('<ul></ul>');
    this.$_element.append(this.$_listElement);
    
    this.$_element.on('scroll', this.onScroll.bind(this));
};

/**
* Загрузка данных в компонент
*/
ListView.prototype.loadData = function (data) {
    var self = this;
    this.data = {};
    data.forEach(function (item) {
        var groupKey = self.getGroupValue(item);
        if (!(groupKey in self.data)) {
            self.data[groupKey] = [];
        }
        self.data[groupKey].push(item);
    });
    this.renderData();
};

/**
* Функция рендеринга данных в компонент
*/
ListView.prototype.renderData = function () {
    var self = this,
        keys = Object.keys(this.data)
            .sort(this.groupItemSortCompare.bind(this));
    keys.forEach(function (key) {
        var $groupEl = $('<div></div>')
            .text(key)
            .addClass('item')
            .addClass('group');

        self.$_listElement.append($groupEl);
        $groupEl.wrap('<li />');
        
        self.data[key]
            .sort(self.itemSortCompare.bind(self))
            .forEach(self.addItem.bind(self));
    });
    this.$_groupElements = this.$_element.find('.item.group');
    this.$_groupElements.each(function (i, item) {
        var $item = $(item);
        $item
            .data('originalPosition', $item.offset().top)
            .parent()
            .height($item.outerHeight());
    });
};

/**
* Предикат для сортировки групповых элементов
*/
ListView.prototype.groupItemSortCompare = function (item1, item2) {
    return (item1 < item2) ? -1 : 1;
};

/**
* Предикат для сортировки негрупповых элементов
*/
ListView.prototype.itemSortCompare = function (item1, item2) {
    return (item1 < item2) ? -1 : 1;
};

/**
* Добавление негруппового элемента в список
*/
ListView.prototype.addItem = function (item) {
    this.$_listElement.append(
        $('<li></li>')
            .append(this.getItemValue(item))
            .addClass('item'));
};

/**
* Составление значения для негруппового элемента
*/
ListView.prototype.getItemValue = function (item) {
    var textItems = [];
    this.displayKeys.forEach(function (key) {
        if (item[key] !== undefined) {
            textItems.push(item[key]);
        }
    });
    return textItems.join(this.displaySeparator)
};

/**
* Функция для вычисления ключа группировки
*/
ListView.prototype.getGroupValue = function (item) {
    return item[this.groupKey] || '';
};

/**
* Обработчик события скроллинга в компоненте
*/
ListView.prototype.onScroll = function (event) {
    var self = this;
    this.$_groupElements.each(function (index, item) {
        var $item = $(item),
        scrollTop = self.$_element.scrollTop(),
        elementTop = self.$_element.position().top + 
            parseInt(self.$_element.css('border-top-width')),
        itemWidth = self.$_listElement.width() - 
            parseInt($item.css('padding-left')) -
            parseInt($item.css('padding-right')),
        itemPosition = $item.data('originalPosition');
        
        if (itemPosition <= scrollTop + elementTop) {
            var $nextItem = self.$_groupElements.eq(index + 1),
                nextItemTop = $nextItem.offset().top - $item.height();
            $item.addClass('absolute').width(itemWidth).offset({'top': elementTop});
            if ($nextItem.length > 0) { 
                if ($item.offset().top >= nextItemTop) {
                    $item.offset({'top': nextItemTop});
                }
            }
        } else {
            $item.removeClass("absolute");
        }
    });
};
