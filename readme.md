﻿# ListView

ListView - список элементов с плавающими заголовками, реализованный с помощью jQuery

## Состав репозитория

- ``listview.js`` - файл с компонентом
- ``listview.css`` - файл со стилями для компонента
- ``main.html`` - демо (для запуска достаточно склонировать репозиторий)
- ``future.md`` - предложения по улучшению

## Использование

1. Подключаем на странице jQuery, класс компонента listview.js и файл со стилями listview.css

```html
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link rel="stylesheet" href="listview.css">
    <script src="listview.js"></script>
</head>
```

2. Создаем на странице элемент, в котором должен располагаться список

```html
<div id="listview_item">
</div>
```

3. Создаем список и загружаем в него список значений

```html
<script>
var listview = new ListView({
        width: 200,  // ширина списка
        height: 250,  // высота списка
        displayKeys: ['name'],  // отображаемые значения
        groupKey: 'group'  // группирующий ключ
    });
$("#listview_item").append(listview.getElement());
listview.loadData([{name: 'Вася', group: 'A'}, {name: 'Ваня', group: 'B'}, {name: 'Петя', group: 'A'}]);
</script>
```

## Кастомизация

Для задания вычисляемой логики группировки наследуется новый класс и переопределяется метод `getGroupValue`

```js
NewListView.prototype.getGroupValue = function (item) {
    // группировка по первой букве имени
    return item['name'] && item['name'][0] || '';
};
```